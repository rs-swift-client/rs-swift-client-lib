//! Well known headers used by swift protocol

header! { (XAuthKey, "X-Auth-Key") => [String] }
header! { (XAuthUser, "X-Auth-User") => [String] }
header! { (XAuthToken, "X-Auth-Token") => [String] }
