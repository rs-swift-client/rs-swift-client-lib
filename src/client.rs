use reqwest;
use ::Error;
use ::headers;

pub struct Client {
    auth_token: Option<String>,
    hostname: Option<String>,
    schema: String
}

#[derive(Debug,Deserialize)]
pub struct ListBucketsEntry {
    pub name: String,
    pub count: u64,
    pub bytes: u64
}

pub type LoginResult = Result<String, Error>;
pub type ListBucketsResult = Result<Vec<ListBucketsEntry>, Error>;

impl Client {
    /// Creates new empty client with default properties
    pub fn new() -> Self {
        Client {
            auth_token: None,
            hostname: None,
            schema: String::from("https")
        }
    }
    
    /// Creates new Client with hostname and auth_token preset
    pub fn with_token(hostname: &str, auth_token: &str) -> Self {
        Client {
            auth_token: Some(auth_token.to_owned()),
            hostname: Some(hostname.to_owned()),
            schema: String::from("https"),
        }
    }

    /// Creates new Client with just hostname set
    pub fn with_hostname(hostname: &str) -> Self {
        Client {
            auth_token: None,
            hostname: Some(hostname.to_owned()),
            schema: String::from("https"),
        }
    }

    pub fn auth_token(&self) -> Option<String> {
        self.auth_token.clone()
    }

    pub fn set_auth_token(&mut self, auth_token: &str) {
        self.auth_token = Some(auth_token.to_owned())
    }

    pub fn hostname(&self) -> Option<String> {
        self.hostname.clone()
    }


    pub fn login(&mut self, username: &str, password: &str) -> LoginResult {
        let mut url_string = String::new();
        let mut url: reqwest::Url;

        url_string.push_str(self.schema.as_str());
        url_string.push_str("://");
        if let Some(ref hostname) = self.hostname {
            url_string.push_str(hostname.as_str());
        } else {
            return Err(Error::new("hostname not set. Set it first!"))
        }

        match reqwest::Url::parse(url_string.as_str()) {
            Ok(parsed_url) => {
                url = parsed_url
            }
            Err(e) => { 
                //FIXME change this error, it uncovers too many internal details.
                //Use something with hostname instead.
                return Err(Error::with_cause(&format!("Failed to parse url {}", url_string), e))
            }
        }

        url.set_path("/auth/v2");

        match reqwest::Client::new().get(url)
            .header(headers::XAuthUser(username.to_owned()))
            .header(headers::XAuthKey(password.to_owned()))
            .send() {
            Ok(mut response) => {
                //FIXME handle status code correctly
                debug!("Status: {}", response.status());
                trace!("Headers: {:?}", response.headers());
                trace!("Text: {}", response.text()?);

                match response.headers().get::<headers::XAuthToken>() {
                    Some(h) => {
                        let auth_token = h.to_string();
                        self.set_auth_token(&auth_token);
                        Ok(auth_token)
                    }
                    _ => return Err(Error::new("No X-Auth-Token found in resonse"))
                }
            }
            Err(e) => {
                debug!("Failed to send http request");
                return Err(Error::with_cause(&format!("Failed to send request"), e))
            }
        }
    }

    pub fn list_buckets(&mut self) -> ListBucketsResult {
        let mut url_string = String::new();
        let mut url: reqwest::Url;

        url_string.push_str(self.schema.as_str());
        url_string.push_str("://");

        if let Some(ref hostname) = self.hostname {
            url_string.push_str(hostname.as_str());
        } else {
            return Err(Error::new("hostname not set. Set it first!"))
        }

        match reqwest::Url::parse(url_string.as_str()) {
            Ok(parsed_url) => {
                url = parsed_url
            }
            Err(e) => { 
                //FIXME change this error, it uncovers too many internal details.
                //Use something with hostname instead.
                return Err(Error::with_cause(&format!("Failed to parse url {}", url_string), e))
            }
        }

        url.set_path("/swift/v1/");
        url.query_pairs_mut()
            .append_pair("format", "json");

        let token: String;

        if let Some(ref t) = self.auth_token {
            token = t.to_string();
        } else {
            return Err(Error::new("auth_token not set. Login first!"))
        }
        
        match reqwest::Client::new().get(url)
            .header(headers::XAuthToken(token.to_string()))
            .send() {
            Ok(mut response) => {
                println!("{}", response.status());
                println!("{:?}", response.headers());


                match response.json::<Vec<ListBucketsEntry>>() {
                    Ok(resp) => {
                        Ok(resp)
                    }
                    Err(e) => {
                        return Err(Error::with_cause("Failed to parse response", e))
                    }
                }
            }
            Err(e) => {
                return Err(Error::with_cause(&format!("Failed to send request"), e))
            }
        }
    }
}
