//! Common error handling for the library

use std;
use std::fmt;
use reqwest;

/// Common error representation
#[derive(Debug)]
pub struct Error {
    desc: String,
    cause: Option<Box<std::error::Error>>
}

impl Error {
    /// Creates new error with description and without cause
    pub fn new(desc: &str) -> Self {
        Error {
            desc: desc.to_owned(),
            cause: None }
    }

    /// Creates new error with description and attached cause
    pub fn with_cause<T: std::error::Error + 'static>(desc: &str, cause: T) -> Self {
        Error {
            desc: desc.to_owned(),
            cause: Some(Box::new(cause))
        }
    }
}

impl std::error::Error for Error {
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if let Some(ref cause) = self.cause {
            write!(f, "{}\n", self.desc)?;
            cause.fmt(f)
        } else {
            write!(f, "{}\n", self.desc)
        }
    }
}

impl From<reqwest::Error> for Error {
    fn from(err: reqwest::Error) -> Self {
        Error::with_cause("From request::Error", err)
    }
}
